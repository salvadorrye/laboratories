# Generated by Django 3.1.6 on 2021-08-25 04:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('laboratories', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='laboratory',
            name='date',
            field=models.DateTimeField(),
        ),
    ]
