# Generated by Django 3.1.6 on 2021-08-24 08:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('geriatrics', '0023_auto_20210609_1130'),
    ]

    operations = [
        migrations.CreateModel(
            name='Laboratory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('resident', models.ForeignKey(limit_choices_to={'discharged': False}, null=True, on_delete=django.db.models.deletion.CASCADE, to='geriatrics.admission')),
            ],
            options={
                'ordering': ('-date',),
            },
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('result', models.CharField(max_length=200)),
                ('laboratory', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratories.laboratory')),
            ],
        ),
    ]
