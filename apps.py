from django.apps import AppConfig


class LaboratoriesConfig(AppConfig):
    name = 'laboratories'
