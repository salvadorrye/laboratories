from django.db import models
from geriatrics.models import Admission

# Create your models here.
class Laboratory(models.Model):
    admission = models.ForeignKey(
                                 Admission,
                                 on_delete=models.CASCADE,
                                 limit_choices_to={'discharged': False},
                                 null=True
                                )
    date = models.DateField(auto_now=False, null=False, blank=False)
    show = models.BooleanField(default=True)

    class Meta:
        ordering = ('date',)
        verbose_name_plural = 'Laboratories'

    def __str__(self):
        return f'{self.admission} - {self.date}'

class Test(models.Model):
    laboratory = models.ForeignKey(Laboratory, on_delete=models.CASCADE)
    name = models.CharField(max_length=200) 
    result = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.name}: {self.result}'
