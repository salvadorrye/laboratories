from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import Laboratory, Test

# Register your models here.
class LaboratoriesAdminSite(AdminSite):
    site_header = 'JHWC Laboratories Admin'
    site_title = 'JHWC Laboratories Admin Portal'
    index_title = 'Welcome to JHWC Laboratories Portal'

laboratories_admin_site = LaboratoriesAdminSite(name='laboratories_admin')

class TestInline(admin.TabularInline):
    model = Test

@admin.register(Laboratory)
class LaboratoryAdmin(admin.ModelAdmin):
    list_display = ['admission', 'date', 'show']
    inlines = [TestInline]

laboratories_admin_site.register(Laboratory, admin_class=LaboratoryAdmin)
